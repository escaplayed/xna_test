﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace escaplayed_test.Sprites
{
    public class Sprite
    {
        protected GraphicsDeviceManager GraphicsDeviceManager { get; set; }

        protected int MinXPosition { get; set; }
        protected int MaxXPosition { get; set; }
        protected int MinYPosition { get; set; }
        protected int MaxYPosition { get; set; }

        public bool IsCollidable { get; set; }

        public Rectangle CollisionBox
        {
            get
            {
                return new Rectangle((int)Position.X, (int)Position.Y, Texture.Width, Texture.Height);
            }
        }

        public Vector2 Position { get; set; }

        private Texture2D _texture;
        public Texture2D Texture
        {
            get { return _texture; }
            set
            {
                _texture = value;

                MinXPosition = 0;
                MaxXPosition = GraphicsDeviceManager.GraphicsDevice.Viewport.Width - value.Width;
                MinYPosition = 0;
                MaxYPosition = GraphicsDeviceManager.GraphicsDevice.Viewport.Height - value.Height;
            }
        }

        public Sprite(GraphicsDeviceManager graphicsDeviceManager)
        {
            GraphicsDeviceManager = graphicsDeviceManager;

            Position = Vector2.Zero;
        }
    }
}
