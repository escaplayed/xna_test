﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using escaplayed_test.Managers.Textures;

namespace escaplayed_test.Sprites.Objects
{
    public class CrateSprite : Sprite
    {
        private CrateTextureManager _textureManager;

        public CrateSprite(GraphicsDeviceManager graphicsDeviceManager, CrateTextureManager textureManager) : base(graphicsDeviceManager)
        {
            base.GraphicsDeviceManager = graphicsDeviceManager;
            _textureManager = textureManager;

            Texture = _textureManager.CrateTexture;
            IsCollidable = true;
        }

        public void Update(GameTime gameTime)
        {

        }
    }
}
