﻿using escaplayed_test.Managers.Collidables;
using escaplayed_test.Managers.Textures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace escaplayed_test.Sprites.Characters
{
    public class PlayerSprite : Sprite
    {
        private PlayerTextureManager _textureManager;

        private KeyboardState _previousKeyboardState;
        private Vector2 _lastValidPosition;

        public Vector2 MovementSpeed { get; set; }

        private FacingDirection _facingDirection;
        public FacingDirection FacingDirection
        {
            get { return _facingDirection; }
            set
            {
                switch (value)
                {
                    case FacingDirection.North:
                        Texture = _textureManager.NorthTexture;
                        break;
                    case FacingDirection.East:
                        Texture = _textureManager.EastTexture;
                        break;
                    case FacingDirection.South:
                        Texture = _textureManager.SouthTexture;
                        break;
                    case FacingDirection.West:
                        Texture = _textureManager.WestTexture;
                        break;
                }

                _facingDirection = value;
            }
        }

        public PlayerSprite(GraphicsDeviceManager graphicsDeviceManager, PlayerTextureManager textureManager) : base(graphicsDeviceManager)
        {
            base.GraphicsDeviceManager = graphicsDeviceManager;
            _textureManager = textureManager;
            _previousKeyboardState = Keyboard.GetState(PlayerIndex.One);
            _lastValidPosition = Position;

            MovementSpeed = new Vector2(150.0f, 150.0f);
            FacingDirection = FacingDirection.North;

            Texture = _textureManager.NorthTexture;
            IsCollidable = true;
        }

        public void Update(GameTime gameTime)
        {
            KeyboardState currentKeyboardState = Keyboard.GetState(PlayerIndex.One);

            ProcessMovementInput(gameTime, currentKeyboardState, Position);

            _previousKeyboardState = currentKeyboardState;
        }

        private void ProcessMovementInput(GameTime gameTime, KeyboardState keyboardState, Vector2 oldPosition)
        {
            float totalSecondsElapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;

            bool playerHasCollided = false;
            Vector2 spritePosition = Position;

            foreach (Sprite collidable in CollidableManager.NearbyCollidables)
            {
                if (!ReferenceEquals(this, collidable) && collidable.IsCollidable && CollisionBox.Intersects(collidable.CollisionBox))
                {
                    playerHasCollided = true;
                    spritePosition = _lastValidPosition;
                    break;
                }
            }

            if (!playerHasCollided)
            {
                _lastValidPosition = spritePosition;
            }

            if ((keyboardState.IsKeyDown(Keys.W) || keyboardState.IsKeyDown(Keys.Up)) && Position.Y > MinYPosition)
            {
                spritePosition.Y -= MovementSpeed.Y * totalSecondsElapsed;
                FacingDirection = FacingDirection.North;
            }

            if ((keyboardState.IsKeyDown(Keys.A) || keyboardState.IsKeyDown(Keys.Left)) && Position.X > MinXPosition)
            {
                spritePosition.X -= MovementSpeed.X * totalSecondsElapsed;
                FacingDirection = FacingDirection.West;
            }

            if ((keyboardState.IsKeyDown(Keys.S) || keyboardState.IsKeyDown(Keys.Down)) && Position.Y < MaxYPosition)
            {
                spritePosition.Y += MovementSpeed.Y * totalSecondsElapsed;
                FacingDirection = FacingDirection.South;
            }

            if ((keyboardState.IsKeyDown(Keys.D) || keyboardState.IsKeyDown(Keys.Right)) && Position.X < MaxXPosition)
            {
                spritePosition.X += MovementSpeed.X * totalSecondsElapsed;
                FacingDirection = FacingDirection.East;
            }

            Position = spritePosition;
        }
    }
}
