using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using escaplayed_test.Sprites.Characters;
using escaplayed_test.Managers.Textures;
using escaplayed_test.Managers.Textures.Interfaces;
using escaplayed_test.Sprites.Objects;
using escaplayed_test.Managers.Collidables;

namespace escaplayed_test
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MainGame : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private IList<ITextureManager> _textureManagers;

        private PlayerSprite _playerSprite;
        private CrateSprite _crateSprite;
        private SpriteFont _mainFont;
        private Texture2D _dummyTexture;

        public MainGame()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            _textureManagers = new List<ITextureManager>();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _mainFont = Content.Load<SpriteFont>("fonts/CourierNew");


            PlayerTextureManager playerTextureManager = new PlayerTextureManager();
            playerTextureManager.LoadTextures(Content);

            CrateTextureManager crateTextureManager = new CrateTextureManager();
            crateTextureManager.LoadTextures(Content);

            _textureManagers.Add(playerTextureManager);
            _textureManagers.Add(crateTextureManager);

            _playerSprite = new PlayerSprite(_graphics, playerTextureManager);
            _playerSprite.FacingDirection = FacingDirection.South;

            //_dummyTexture = new Texture2D(GraphicsDevice, _playerSprite.Texture.Width, _playerSprite.Texture.Height, false, SurfaceFormat.Color);
            //Color[] color = new Color[_playerSprite.Texture.Width * _playerSprite.Texture.Height];
            //for (int i = 0; i < color.Length; ++i)
            //    color[i] = Color.White;

            //_dummyTexture.SetData(color);

            _crateSprite = new CrateSprite(_graphics, crateTextureManager);
            _crateSprite.Position = new Vector2(100.0f, 100.0f);

            CollidableManager.NearbyCollidables.Add(_playerSprite);
            CollidableManager.NearbyCollidables.Add(_crateSprite);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            foreach (ITextureManager textureManager in _textureManagers)
            {
                textureManager.Dispose();
            }

            Content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyboardState = Keyboard.GetState(PlayerIndex.One);
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);

            if (keyboardState.IsKeyDown(Keys.Escape) || gamePadState.Buttons.Back == ButtonState.Pressed)
                this.Exit();

            _playerSprite.Update(gameTime);

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);

            _spriteBatch.Draw(_playerSprite.Texture, _playerSprite.Position, Color.White);
            _spriteBatch.Draw(_crateSprite.Texture, _crateSprite.Position, Color.White);

            //_spriteBatch.Draw(_dummyTexture, _playerSprite.Position, Color.White);

            const string text = @"Jake is a prick";
            Vector2 fontCentre = _mainFont.MeasureString(text) / 2;
            Vector2 fontPos = new Vector2(_graphics.GraphicsDevice.Viewport.Width / 2, _graphics.GraphicsDevice.Viewport.Height / 2);
            _spriteBatch.DrawString(_mainFont, text, fontPos, Color.DimGray, 0, fontCentre, 1.0f, SpriteEffects.None, 0.5f);

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
