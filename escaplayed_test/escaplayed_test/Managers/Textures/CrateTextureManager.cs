﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace escaplayed_test.Managers.Textures
{
    public class CrateTextureManager : Interfaces.ITextureManager
    {
        public Texture2D CrateTexture { get; set; }

        private IList<IDisposable> _texturesToDispose;

        public void LoadTextures(ContentManager content)
        {
            CrateTexture = content.Load<Texture2D>("objects/crate");

            _texturesToDispose = new List<IDisposable>()
            {
                CrateTexture
            };
        }

        public void Dispose()
        {
            foreach (IDisposable texture in _texturesToDispose)
            {
                if (texture != null)
                {
                    texture.Dispose();
                }
            }
        }
    }
}
