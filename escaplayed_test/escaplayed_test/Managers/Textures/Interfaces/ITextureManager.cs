﻿using System;

namespace escaplayed_test.Managers.Textures.Interfaces
{
    internal interface ITextureManager : IDisposable
    {
        void LoadTextures(Microsoft.Xna.Framework.Content.ContentManager content);
    }
}
