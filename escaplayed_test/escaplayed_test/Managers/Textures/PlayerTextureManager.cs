﻿using System;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace escaplayed_test.Managers.Textures
{
    public class PlayerTextureManager : Interfaces.ITextureManager
    {
        public Texture2D NorthTexture { get; set; }
        public Texture2D EastTexture { get; set; }
        public Texture2D SouthTexture { get; set; }
        public Texture2D WestTexture { get; set; }

        private IList<IDisposable> _texturesToDispose;

        public void LoadTextures(ContentManager content)
        {
            NorthTexture = content.Load<Texture2D>("character/N");
            EastTexture = content.Load<Texture2D>("character/E");
            SouthTexture = content.Load<Texture2D>("character/S");
            WestTexture = content.Load<Texture2D>("character/W");

            _texturesToDispose = new List<IDisposable>()
            {
               NorthTexture,
               EastTexture,
               SouthTexture,
               WestTexture
            };
        }

        public void Dispose()
        {
            foreach (IDisposable texture in _texturesToDispose)
            {
                if (texture != null)
                {
                    texture.Dispose();
                }
            }
        }
    }
}
