﻿using System.Collections.Generic;
using escaplayed_test.Sprites;

namespace escaplayed_test.Managers.Collidables
{
    public static class CollidableManager
    {
        public static IList<Sprite> NearbyCollidables { get; set; }

        static CollidableManager()
        {
            NearbyCollidables = new List<Sprite>();
        }
    }
}
